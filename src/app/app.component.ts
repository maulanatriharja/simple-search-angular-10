import { AbstractType, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { debounce } from 'lodash';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'simple-search';

  data: any = [];

  cari_value: string = '';

  constructor(private http: HttpClient) { 
    this.load_data = debounce(this.load_data, 600)
  }

  ngOnInit() {
    this.load_data('');
  }

  load_data(event: any) {

    if (event.target != undefined) {
      this.cari_value = event.target.value;
    } else {
      this.cari_value = '';
    }

    var url = 'https://cors-anywhere.herokuapp.com/https://jobs.github.com/positions.json?search='; // this url for CORS Problem
    // var url = 'https://jobs.github.com/positions.json?search='; // normal url


    this.http.get(url + this.cari_value).subscribe((data) => {
      this.data = data,1000;

    }, error => {
      alert("Something wrong. " + JSON.stringify(error.error));
    });
  }
}
